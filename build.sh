#!/bin/bash
NDKPath="/opt/android-ndk"
buildScript="$NDKPath/build/ndk-build"
$buildScript NDK_PROJECT_PATH=$(pwd) APP_BUILD_SCRIPT=$(pwd)/Android.mk NDK_APPLICATION_MK=$(pwd)/Application.mk && adb push ./libs/arm64-v8a/*.so /sdcard/Android/data/com.beatgames.beatsaber/files/mods/ && echo "Mod succeessfully compiled and pushed!"
rm redbar.zip
cp ./libs/arm64-v8a/*.so .
zip redbar.zip bmbfmod.json *.so
rm ./*.so