#include "../include/main.hpp"
rapidjson::Document& config_doc = Configuration::config;

static struct Config_t {
    bool Rainbows = false;
    bool Fadeout = false;
    float Alpha = 1.0;
    float DiehpR = 1.0;
    float DiehpG = 0.0;
    float DiehpB = 0.0;
    float LowhpR = 1.0;
    float LowhpG = 1.0;
    float LowhpB = 0.0;
    float MidhpR = 0.0;
    float MidhpG = 1.0;
    float MidhpB = 1.0;
    float HighpR = 1.0;
    float HighpG = 1.0;
    float HighpB = 1.0;
    float DefhpR = 1.0;
    float DefhpG = 1.0;
    float DefhpB = 1.0;
} Config;

Il2CppObject* energyBarMaterialStore = nullptr;
Il2CppObject* energyBarStore = nullptr;
float energyy = 0.0;
int pos = 0;

MAKE_HOOK_OFFSETLESS(HandleGameEnergyDidChange, void, Il2CppObject* self, float energy) {
    Color color;
    color.a = Config.Alpha;
    color.r = Config.DefhpR;
    color.g = Config.DefhpG;
    color.b = Config.DefhpB;;
    Il2CppObject* energyBar;
    il2cpp_utils::GetFieldValue(&energyBar, self, "_energyBar");
    Il2CppObject* energyBarMaterial;
    il2cpp_utils::GetFieldValue(&energyBarMaterial, energyBar, "material");
    if (energyBarMaterialStore != energyBarMaterial || energyBarStore != energyBar) {
        energyBarMaterialStore = energyBarMaterial;
        energyBarStore = energyBar;
    }
    if (energy != energyy) {
        energyy = energy;
    }
    HandleGameEnergyDidChange(self, energy);
    if (energy < 0.15) {
        color.r = Config.DiehpR; color.g = Config.DiehpG; color.b = Config.DiehpB;
    } else if (energy < 0.5) {
        color.r = (Config.LowhpR != 0.0) ? Config.LowhpR : (-0.429+2.857*energy)*Config.DefhpR;
        color.g = (Config.LowhpG != 0.0) ? Config.LowhpG : (-0.429+2.857*energy)*Config.DefhpG;
        color.b = (Config.LowhpB != 0.0) ? Config.LowhpB : (-0.429+2.857*energy)*Config.DefhpB;
    } else if (energy == 1.0) {
        if (!Config.Rainbows) {
            color.r = Config.HighpR; color.b = Config.HighpB; color.g = Config.HighpG;
        }
    } else if (energy > 0.7) {
        color.r = (Config.MidhpR != 0.0) ? Config.MidhpR : 3.333+(-3.333*energy)*Config.DefhpR;
        color.g = (Config.MidhpG != 0.0) ? Config.MidhpG : 3.333+(-3.333*energy)*Config.DefhpG;
        color.b = (Config.MidhpB != 0.0) ? Config.MidhpB : 3.333+(-3.333*energy)*Config.DefhpB;
    }
    if (energy > 0.85 && Config.Fadeout) {
        color.a = (6.667+(-6.667*energy))*Config.Alpha;
    } else {
        pos = 0;
        color.a = Config.Alpha;
    }
    il2cpp_utils::RunMethod(energyBar, "set_color", color); 
}

//stolen from some random pastebin, thanks, whoever you are!
float * Wheel(int WheelPos) {
  static int c[3];
  static float epic[3];
 
  if(WheelPos < 85) {
   c[0]=WheelPos * 3;
   c[1]=255 - WheelPos * 3;
   c[2]=0;
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   c[0]=255 - WheelPos * 3;
   c[1]=0;
   c[2]=WheelPos * 3;
  } else {
   WheelPos -= 170;
   c[0]=0;
   c[1]=WheelPos * 3;
   c[2]=255 - WheelPos * 3;
  }

  epic[0] = (float) c[0] / 255.0;
  epic[1] = (float) c[1] / 255.0;
  epic[2] = (float) c[2] / 255.0;
 
  return epic;
}

MAKE_HOOK_OFFSETLESS(UpdateLol, void, Il2CppObject* self) {
    UpdateLol(self);
    if (energyBarMaterialStore != nullptr && energyy == 1.0 && Config.Rainbows) {
        float *Heck = Wheel(pos);
        Color color;
        color.a = 1.0;
        color.r = Heck[0];
        color.g = Heck[1];
        color.b = Heck[2];
        il2cpp_utils::RunMethod(energyBarStore, "set_color", color); 
        pos++;
        if (pos > 255) {
            pos = 0;
        }
    }
}

void SaveSettings() { //copied a lot of code from rainbowmod for this: thank you, darknight1050!!!!
    log(INFO, "Redbar: Saving configuration...");
    config_doc.RemoveAllMembers();
    config_doc.SetObject();
    rapidjson::Document::AllocatorType& allocator = config_doc.GetAllocator();
    config_doc.AddMember("Rainbows", Config.Rainbows, allocator);
    config_doc.AddMember("Fadeout", Config.Fadeout, allocator);
    config_doc.AddMember("Alpha", Config.Alpha, allocator);
    config_doc.AddMember("DiehpR", Config.DiehpR, allocator);
    config_doc.AddMember("DiehpG", Config.DiehpG, allocator);
    config_doc.AddMember("DiehpB", Config.DiehpB, allocator);
    config_doc.AddMember("LowhpR", Config.LowhpR, allocator);
    config_doc.AddMember("LowhpG", Config.LowhpG, allocator);
    config_doc.AddMember("LowhpB", Config.LowhpB, allocator);
    config_doc.AddMember("MidhpR", Config.MidhpR, allocator);
    config_doc.AddMember("MidhpG", Config.MidhpG, allocator);
    config_doc.AddMember("MidhpB", Config.MidhpB, allocator);
    config_doc.AddMember("HighpR", Config.HighpR, allocator);
    config_doc.AddMember("HighpG", Config.HighpG, allocator);
    config_doc.AddMember("HighpB", Config.HighpB, allocator);
    config_doc.AddMember("DefhpR", Config.DefhpR, allocator);
    config_doc.AddMember("DefhpG", Config.DefhpG, allocator);
    config_doc.AddMember("DefhpB", Config.DefhpB, allocator);
    Configuration::Write();
    log(INFO, "Redbar: Done saving configuration!");
}

bool LoadSettings() { //is there a better way to do this . . . ? rip in sphagetti, wew.
    Configuration::Load();
    bool foundEverything = true;
    if(config_doc.HasMember("Rainbows") && config_doc["Rainbows"].IsBool()){
        Config.Rainbows = config_doc["Rainbows"].GetBool();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("Fadeout") && config_doc["Fadeout"].IsBool()){
        Config.Fadeout = config_doc["Fadeout"].GetBool();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("Alpha") && config_doc["Alpha"].IsFloat()){
        Config.Alpha = config_doc["Alpha"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("DiehpR") && config_doc["DiehpR"].IsFloat()){
        Config.DiehpR = config_doc["DiehpR"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("DiehpG") && config_doc["DiehpG"].IsFloat()){
        Config.DiehpG = config_doc["DiehpG"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("DiehpB") && config_doc["DiehpB"].IsFloat()){
        Config.DiehpB = config_doc["DiehpB"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("LowhpR") && config_doc["LowhpR"].IsFloat()){
        Config.LowhpR = config_doc["LowhpR"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("LowhpG") && config_doc["LowhpG"].IsFloat()){
        Config.LowhpG = config_doc["LowhpG"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("LowhpB") && config_doc["LowhpB"].IsFloat()){
        Config.LowhpB = config_doc["LowhpB"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("MidhpR") && config_doc["MidhpR"].IsFloat()){
        Config.MidhpR = config_doc["MidhpR"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("MidhpG") && config_doc["MidhpG"].IsFloat()){
        Config.MidhpG = config_doc["MidhpG"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("MidhpB") && config_doc["MidhpB"].IsFloat()){
        Config.MidhpB = config_doc["MidhpB"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("HighpR") && config_doc["HighpR"].IsFloat()){
        Config.HighpR = config_doc["HighpR"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("HighpG") && config_doc["HighpG"].IsFloat()){
        Config.HighpG = config_doc["HighpG"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("HighpB") && config_doc["HighpB"].IsFloat()){
        Config.HighpB = config_doc["HighpB"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("DefhpR") && config_doc["DefhpR"].IsFloat()){
        Config.DefhpR = config_doc["DefhpR"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("DefhpG") && config_doc["DefhpG"].IsFloat()){
        Config.DefhpG = config_doc["DefhpG"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(config_doc.HasMember("DefhpB") && config_doc["DefhpB"].IsFloat()){
        Config.DefhpB = config_doc["DefhpB"].GetFloat();    
    }else{
        foundEverything = false;
    }
    if(foundEverything){
        log(INFO, "Redbar: Loaded configuration!");
        return true;
    }
    return false;
}

/*__attribute__((constructor)) void lib_main()
{
    log(INFO, "Hello from redbar :D");
}*/

extern "C" void load() {
    log(INFO, "Redbar: Loading configuration...");
    if (!LoadSettings()) {
        SaveSettings();
    }
    log(INFO, "Redbar: Installing hooks...");
    INSTALL_HOOK_OFFSETLESS(HandleGameEnergyDidChange, il2cpp_utils::FindMethodUnsafe("", "GameEnergyUIPanel", "HandleGameEnergyDidChange", 1));
    INSTALL_HOOK_OFFSETLESS(UpdateLol, il2cpp_utils::FindMethodUnsafe("", "GameEnergyCounter", "Update", 0));
    log(INFO, "Redbar: Finished loading! :D");
    log(INFO, "Redbar: Also, redbar says trans rights.");
}