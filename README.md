# Redbar

Does funky stuff to your energy bar in beat saber!

[Download latest version now!](https://files.catbox.moe/ri9ku6.zip)

## Features

* Your energy bar becomes diffrent colors on diffrent energy levels!
* Fancy gradients as you lose or gain energy
* Configurable through simple json file
* Make the energy bar become a rainbow at max health
* Or make it fade out and disappear

## Config file

* Rainbow (true/false) = Rainbow energy bar at max health?
* Fadeout (true/false) = Fade the energy bar out when almost at max health?
* Alpha (float 0.0-1.0) = Transparency of energy bar
* XXXhpR/G/B (float 0.0-1.0) = Color for diffrent levels on the energy bar

# Downloads and changelog

* Version 1.1.2 (Latest!) - [Download](https://files.catbox.moe/ri9ku6.zip) - Fix some bug, set support for 1.8.0
* Version 1.1.1 - [Download](https://files.catbox.moe/s0oy59.zip) - Rainbow always starts at green.
* Version 1.1.0 - [Download](http://nazr.in/1dkE) - Add a config file, rainbows, gradients, pretty much everything.
* Version 1.0.0 - [Download](http://nazr.in/1dkA) - Initial release, no gradients, no config, no nothing.

## Credits

* [Sc2ad](https://github.com/Sc2ad) and [jakibaki](https://github.com/jakibaki) - [beatsaber-hook](https://github.com/sc2ad/beatsaber-hook)
* [raftario](https://github.com/raftario) - [vscode-bsqm](https://github.com/raftario/vscode-bsqm) and [this template](https://github.com/raftario/bmbf-mod-template)
